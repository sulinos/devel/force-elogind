DESTDIR=
LIBDIR=lib
PKGCONFIGDIR=usr/lib/pkgconfig
INCLUDEDIR=usr/include

gendir:
	mkdir -p $(DESTDIR)/$(LIBDIR) 2>/dev/null || true
	mkdir -p $(DESTDIR)/$(PKGCONFIGDIR) 2>/dev/null || true
	mkdir -p $(DESTDIR)/$(INCLUDEDIR) 2>/dev/null || true

install:
	install src/libsystemd.pc $(DESTDIR)/$(PKGCONFIGDIR)/libsystemd.pc
	install src/systemd.pc $(DESTDIR)/$(PKGCONFIGDIR)/systemd.pc
	ln -s libelogind.so.0 $(DESTDIR)/$(LIBDIR)/libsystemd.so.0
	ln -s libelogind.so.0 $(DESTDIR)/$(LIBDIR)/libsystemd.so.0.25.0
	ln -s libelogind.so $(DESTDIR)/$(LIBDIR)/libsystemd.so
	ln -s elogind $(DESTDIR)/$(INCLUDEDIR)/systemd
	
remove:
	rm -f $(DESTDIR)/$(PKGCONFIGDIR)/libsystemd.pc
	rm -f $(DESTDIR)/$(PKGCONFIGDIR)/systemd.pc
	rm -f $(DESTDIR)/$(LIBDIR)/libsystemd.so.0
	rm -f $(DESTDIR)/$(LIBDIR)/libsystemd.so.0.25.0
	rm -f $(DESTDIR)/$(LIBDIR)/libsystemd.so
	rm -f $(DESTDIR)/$(INCLUDEDIR)/systemd

check:
	cd test ;	bash build.sh
	ldd test/test | grep libelogind
	ldd test/test2 | grep libelogind
	ldd test/test3 | grep libelogind
	ldd test/test4 | grep libelogind
