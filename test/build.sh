#build with pkgconfig
gcc -o test `pkg-config --cflags --libs libsystemd` test.c
#build with force link libsystemd
gcc -o test2 -lsystemd `pkg-config --cflags --libs libsystemd` test.c

#build without pkgconfig
gcc -o test3 -lelogind test2.c
#build with force link libsystemd without pkgconfig
gcc -o test4 -lsystemd test2.c

